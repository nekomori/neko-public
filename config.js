let config = {
    endpoints: {
        api: 'https://api.nekomori.ch',
        rip: 'https://rip.nekomori.ch'
    },
    shiki: 'https://shikimori.one/oauth/authorize?client_id=2dne2S0Df1KVyOvuUeP_KgKN-y5hoenVYRRBeTb9y-w&redirect_uri=https%3A%2F%2Fapi.nekomori.ch%2Fauth&response_type=code&scope='
}

let enums = {
    languages: ['Неизвестный', 'Японский', 'Английский', 'Русский', 'Немецкий', 'Украинский', 'Китайский', 'Ромадзи'],
    player: {
	    sources: ['Все', 'Оригинал', 'BlueRay'],
	    kinds: ['None', 'Оригинал', 'Субтитры', 'Озвучка'],
	    statuses: ['None', 'Удаленный', 'Сомнительный', 'Trustish', 'Проверенный', 'Модерированый', 'Абсолютный'],
    },
    user: {
        lists: ['none', 'watching', 'watched', 'next', 'maybe', 'ban', 'bin', 'drop'],
        listMap: {
            none: 'Добавить в список',
            watching: 'Смотрю',
            watched: 'Просмотрено',
            next: 'Следующее',
            maybe: 'Возможно',
            ban: 'Неинтересно',
            bin: 'Отложено',
            drop: 'Брошено',
        },
        rates: ['none', 'trash', 'hmm', 'good', 'holyshi'],
        rateMap: {
            none: 'Нет оценки',
            trash: 'Мусор',
            hmm: 'Проходняк',
            good: 'Похвально',
            holyshi: 'Изумительно',
        }
    },
    wiki: {
        kinds: ['Неизвестно', 'TV Сериал', 'Фильм', 'ONA', 'OVA', 'Спешл', 'Клип', 'Новелла', 'Манга', 'Ваншот', 'Додзинси'],
        kindMasks: {
            anime: [0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
            manga: [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
        },
    }
}
